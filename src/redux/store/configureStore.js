import { createStore, combineReducers } from "redux";
import user from "../reducers/userReducer";
import projects from "../reducers/projectReducers";
import {
  loadStateFromSessionStorage as loadFromSS,
  saveStateToSessionStorage as SaveToSS,
} from "../sessionStorage/sessionStorage";

export const store = createStore(
  combineReducers({
    projects,
    user,
  }),
  {
    projects: loadFromSS().projects,
    user: loadFromSS().user,
  },
  // a line of code to be able to use Redux DevTool Extension
  process.env.NODE_ENV === "development" &&
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() => {
  // console.log(
  //   "state before saving it into session storage - configureStore.js: ",
  //   store.getState()
  // );

  SaveToSS(store.getState());
});

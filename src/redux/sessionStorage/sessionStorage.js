//-------------------------------------------------
// Load State From Session Storage
//-------------------------------------------------

export const loadStateFromSessionStorage = () => {
  try {
    const serializedState = sessionStorage.getItem("data");
    // console.log("serializedState from sessionStorage.js:", serializedState);

    // if there is no data in session storage, returns an empty array
    if (serializedState === null) {
      return [];
    }

    return JSON.parse(serializedState);
  } catch (error) {
    console.log(error);

    return [];
  }
};

//-------------------------------------------------
// Save State to session Storage
//-------------------------------------------------
export const saveStateToSessionStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);

    sessionStorage.setItem("data", serializedState);
  } catch (error) {
    console.log(error);
  }
};

const initialState = {};

export default (state = initialState, action = []) => {
  switch (action.type) {
    case "save_user":
      return { ...state, ...action.payload };

    default:
      return state;
  }
};

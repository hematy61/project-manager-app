import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import App from "./App.jsx";
import * as serviceWorker from "./serviceWorker";

// dnd library providers for drag and drop
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { TouchBackend } from "react-dnd-touch-backend";

ReactDOM.render(
  <DndProvider backend={HTML5Backend}>
    <DndProvider backend={TouchBackend}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </DndProvider>
  </DndProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

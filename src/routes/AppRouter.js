import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "../pages/Login/Login.jsx";
import Signup from "../pages/Signup/Signup.jsx";
import PrivateRoute from "./PrivateRoute.js";
import Dashboard from "../pages/Dashboard/Dashboard.jsx";
import AddOrEditProject from "../components/AddProject/AddProject.jsx";
import EditProject from "../components/EditProject.js/EditProject.jsx";
import Todos from "../components/ToDos/Todos.jsx";

const AppRouter = () => (
  <Router>
    <Switch>
      <Route path="/login" render={() => <Login />} />
      <Route path="/signup" render={() => <Signup />} />
      <PrivateRoute>
        <Route exact path="/" render={() => <Dashboard />} />
        <Route path="/add" render={() => <AddOrEditProject />} />
        <Route path="/edit/:id" render={() => <EditProject />} />
        <Route path="/project/:id" render={() => <Todos />} />
      </PrivateRoute>
    </Switch>
  </Router>
);

export default AppRouter;

import React from "react";
import { Route, Redirect } from "react-router-dom";
import { loadStateFromSessionStorage as loadFromSS } from "../redux/sessionStorage/sessionStorage";

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        fakeAuth.isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export const fakeAuth = {
  isAuthenticated:
    loadFromSS().user && loadFromSS().user.isLoggedIn ? true : false,
  authenticate(cb) {
    if (loadFromSS().user.isLoggedIn) {
      fakeAuth.isAuthenticated = true;
      cb();
    }
  },
  signout(cb) {
    // saveToSS()
    fakeAuth.isAuthenticated = false;
  },
};

export default PrivateRoute;

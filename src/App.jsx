import React from "react";
import "./App.css";
import AppRouter from "./routes/AppRouter";

// Redux
import { Provider } from "react-redux";
import { store } from "./redux/store/configureStore";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <AppRouter />
      </Provider>
    </div>
  );
}

export default App;

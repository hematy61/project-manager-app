import React, { useRef } from "react";
import { DnDItemTypes } from "../../utils/dndItemTypes";
import { useDrag, useDrop } from "react-dnd";
import { close_icon, todo_text } from "./TodoItem.module.css";
import CheckMarkIcon from "../../assets/icons/checkmark";

export default function TodoItem(props) {
  // refs
  const ref = useRef(null);

  // props destructuring
  const {
    todo,
    index,
    removeToDo,
    editTodo,
    moveProject,
    updateIsCompleted,
  } = props;

  // handle project drop ------------------------------------
  const [{ isOver }, drop] = useDrop({
    accept: DnDItemTypes.TODO_ITEM,
    hover: (item, monitor) => {
      if (!ref.current) {
        return;
      }

      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }

      moveProject(dragIndex, hoverIndex);

      item.index = hoverIndex;
    },
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  });

  // handle project drag ------------------------------------
  const [{ isDragging }, drag] = useDrag({
    item: { type: DnDItemTypes.TODO_ITEM, index },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  drag(drop(ref));

  return (
    <div
      ref={ref}
      style={{
        opacity: isDragging || isOver ? 0.3 : 1,
        cursor: isDragging ? "move" : "default",
      }}
    >
      <li>
        <div
          className={`bg-danger text-white ${close_icon}`}
          onClick={() => removeToDo(index)}
        >
          <span aria-hidden="true">&times;</span>
        </div>

        <input
          className={todo_text}
          type="text"
          value={todo.name}
          onChange={(e) => editTodo(e.target.value, index)}
        />

        <div style={{ fill: todo.isCompleted ? "green" : "grey" }}>
          <CheckMarkIcon onClick={() => updateIsCompleted(todo, index)} />
        </div>
      </li>
    </div>
  );
}

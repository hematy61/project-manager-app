import React, { useState, useCallback } from "react";
import { connect, useDispatch } from "react-redux";
import { useParams, Link } from "react-router-dom";
import { container, header, add_todo, controllers } from "./Todos.module.css";
import TodoItem from "./TodoItem";

const Todos = (props) => {
  // hooks
  const params = useParams();
  const dispatch = useDispatch();

  // route params destructuring
  const id = Number(params.id);

  // finding the target project
  const project = props.projects.filter((x) => x.id === id)[0];

  // states
  const [toDo, setToDo] = useState("");

  // ----------------------------------------
  const getPayloadReady = useCallback(() => {
    return props.projects.map((item) => {
      if (item.id === id) {
        return project;
      }
      return item;
    });
  }, [props.projects, id, project]);

  // ----------------------------------------
  const AddToDo = (e) => {
    e.preventDefault();

    // catching empty string submits
    if (!toDo) {
      return;
    }

    project.todos.push({ name: toDo, isCompleted: false });

    dispatch({ type: "add_todo", payload: getPayloadReady() });
  };

  // ----------------------------------------
  const removeToDo = (idx) => {
    const filterdTodos = project.todos.filter((x, i) => i !== idx);
    project.todos = filterdTodos;

    dispatch({ type: "remove_a_todo", payload: getPayloadReady() });
  };

  // ----------------------------------------
  const editTodo = (value, idx) => {
    const project = props.projects.filter((x) => x.id === id)[0];
    project.todos[idx] = { ...project.todos[idx], name: value };

    dispatch({ type: "edit_a_todo", payload: getPayloadReady(project) });
  };

  // ----------------------------------------
  const moveProject = useCallback(
    (dragIndex, hoverIndex) => {
      const draggedTodo = project.todos[dragIndex];
      const filteredTodos = project.todos.filter((x, i) => dragIndex !== i);

      project.todos = [
        ...filteredTodos.slice(0, hoverIndex),
        draggedTodo,
        ...filteredTodos.slice(hoverIndex),
      ];

      dispatch({
        type: "move_todos",
        payload: getPayloadReady(),
      });
    },
    [project, dispatch, getPayloadReady]
  );

  const handleSort = (e) => {
    const sortBy = e.target.innerText;
    if (sortBy === "Alphabet(A-Z)") {
      project.todos.sort((a, b) => {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        } else if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    } else if (sortBy === "Alphabet(Z-A)") {
      project.todos.sort((a, b) => {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA > nameB) {
          return -1;
        } else if (nameA < nameB) {
          return 1;
        }
        return 0;
      });
    }

    dispatch({
      type: "sort_todos",
      payload: getPayloadReady(),
    });
  };

  const updateIsCompleted = (newToDo, idx) => {
    newToDo.isCompleted = !newToDo.isCompleted;
    project.todos[idx] = newToDo;

    dispatch({
      type: "update_todo",
      payload: getPayloadReady(),
    });
  };

  // ----------------------------------------
  return (
    <div className={container}>
      <div className={header}>
        <h1>{project.name}</h1>
      </div>

      {/* Sort By and Back Buttons ------------------------------- */}
      <div className={controllers}>
        <div className="dropdown">
          <button
            className="btn btn-secondary dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort By
          </button>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <p className="dropdown-item" onClick={handleSort}>
              Alphabet(A-Z)
            </p>
            <p className="dropdown-item" onClick={handleSort}>
              Alphabet(Z-A)
            </p>
          </div>
        </div>

        <Link to="/">
          <button type="button" className="btn btn-primary">
            Back
          </button>
        </Link>
      </div>

      {/* Add ToDo----------------------------------------- */}
      <form onSubmit={AddToDo}>
        <div className={`${add_todo}`}>
          <label htmlFor="add-todo">Add a ToDo: </label>
          <input
            id="add-todo"
            type="text"
            placeholder="Enter a new ToDo"
            aria-label="Add ToDo"
            onChange={(e) => setToDo(e.target.value)}
          />
        </div>
      </form>

      {/* ToDo Items ---------------------------------------- */}
      <ul>
        {project.todos.map((todo, i) => (
          <TodoItem
            key={i}
            todo={todo}
            index={i}
            removeToDo={removeToDo}
            editTodo={editTodo}
            moveProject={moveProject}
            updateIsCompleted={updateIsCompleted}
          />
        ))}
      </ul>
    </div>
  );
};

const mapStateToProps = (state) => ({ projects: state.projects });
export default connect(mapStateToProps)(Todos);

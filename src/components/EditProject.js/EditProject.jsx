import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { useParams, Link, useHistory } from "react-router-dom";
import { container, form_item, button_group } from "./EditProject.module.css";

function EditProject(props) {
  // hooks
  const params = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  // route params
  const id = Number(params.id);
  const project = props.projects.filter((x) => x.id === id)[0];

  // states
  const [projectName, setProjectName] = useState(project.name);

  // --------------------------------------------
  const handleEditSubmit = (e) => {
    e.preventDefault();

    project.name = projectName;

    const data = props.projects.map((item) => {
      if (item.id === id) {
        return project;
      }
      return item;
    });

    dispatch({ type: "update_project", payload: data });

    history.push("/");
  };

  return (
    <div className={container}>
      <h3>Edit Project</h3>
      <form onSubmit={handleEditSubmit}>
        <div className={form_item}>
          <label htmlFor="edit_project">Project Name:</label>
          <input
            type="text"
            id="edit_project"
            aria-describedby="emailHelp"
            value={projectName}
            onChange={(e) => setProjectName(e.target.value)}
          />
        </div>

        <div className={form_item}>
          <label htmlFor="edit_user">User: </label>
          <input type="text" id="edit_user" disabled placeholder="disabled " />
        </div>

        <div className={button_group}>
          <Link to="/" className="btn btn-light">
            Cancel
          </Link>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={handleEditSubmit}
          >
            Add
          </button>
        </div>
      </form>
    </div>
  );
}

const mapStateToProps = (state) => ({ projects: state.projects });
export default connect(mapStateToProps)(EditProject);

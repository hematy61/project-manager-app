import React from "react";
import styles from "./Alert.module.css";

const Alert = (props) => {
  return (
    <div
      className={`alert ${props.color} ${styles.notification} ${styles.show_alert}`}
      role="alert"
    >
      {props.content}
    </div>
  );
};

export default Alert;

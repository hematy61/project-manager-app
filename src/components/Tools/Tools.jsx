import React from "react";
import { tools_container } from "./Tools.module.css";
import { Link } from "react-router-dom";

export const Tools = (props) => {
  const { onRemoveAll, handleSort } = props;

  return (
    <div className={tools_container}>
      <button onClick={onRemoveAll} type="button" className="btn btn-danger">
        Remove All
      </button>

      <Link to="/add">
        <button type="button" className="btn btn-primary">
          Add A New Project
        </button>
      </Link>

      <div className="dropdown">
        <button
          className="btn btn-secondary dropdown-toggle"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          Sort By
        </button>

        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <p className="dropdown-item" onClick={handleSort}>
            Project Name(A-Z)
          </p>
          <p className="dropdown-item" onClick={handleSort}>
            Project Name(Z-A)
          </p>
        </div>
      </div>
    </div>
  );
};

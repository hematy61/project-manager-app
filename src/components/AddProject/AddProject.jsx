import React, { useState } from "react";
import { container } from "./AddProject.module.css";

import { connect, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";

function AddProject(props) {
  // hooks
  const dispatch = useDispatch();
  const history = useHistory();

  // states
  const [projectName, setProjectName] = useState("");

  // ----------------------------------------------
  const handleSubmit = (e) => {
    e.preventDefault();
    e.persist();

    if (!projectName) {
      return;
    }

    const data = props.projects;

    // finding max project ids and user ids
    data.push({
      id:
        data.length === 0
          ? 1
          : Math.max(...data.map((project) => (project.id ? project.id : 0))) +
            1,
      user_id:
        data.length === 0
          ? 1
          : Math.max(...data.map((project) => project.user_id)) + 1,
      name: projectName,
      todos: [],
    });

    dispatch({ type: "add_new_project", payload: data });

    // reroute to Dashboard after updating state
    history.push("/");
  };

  return (
    <div className={container}>
      <h3>Add Project</h3>

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="project-name">Project Name:</label>
          <input
            type="text"
            className="form-control"
            id="project-name"
            aria-describedby="emailHelp"
            onChange={(e) => setProjectName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="user">User: </label>
          <input type="text" className="form-control" id="user" disabled />
        </div>

        <Link to="/">
          <button type="button" className="btn btn-primary">
            Cancel
          </button>
        </Link>

        <button type="submit" className="btn btn-primary">
          Add
        </button>
      </form>
    </div>
  );
}

const mapStateToProps = (state) => ({ projects: state.projects });
export default connect(mapStateToProps)(AddProject);

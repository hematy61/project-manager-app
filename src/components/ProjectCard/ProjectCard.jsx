import React, { useRef } from "react";
import { connect, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { useDrag, useDrop } from "react-dnd";
import { DnDItemTypes } from "../../utils/dndItemTypes";
import {
  project_container,
  user_icon,
  card_title,
  delete_btn,
  edit_btn,
} from "./ProjectCard.module.css";

function Project(props) {
  // hooks
  const dispatch = useDispatch();
  const ref = useRef(null);

  // props destructuring
  let { name = "Default", todos = [], id = 0, user_id = 0 } = props.project;
  const { index, moveProject } = props;

  // handle project drop ------------------------------------
  const [{ isOver }, drop] = useDrop({
    accept: DnDItemTypes.CARD,
    hover: (item, monitor) => {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      moveProject(dragIndex, hoverIndex);

      item.index = hoverIndex;
    },
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  });

  // handle project drag ------------------------------------
  const [{ isDragging }, drag] = useDrag({
    item: { type: DnDItemTypes.CARD, id, index },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  drag(drop(ref));

  // removing one project based on the id
  const RemoveProject = (projectId) => {
    const data = props.projects.filter((project) => project.id !== projectId);
    dispatch({ type: "remove_one_project", payload: data });
  };

  return (
    <div>
      <Link
        to={`/project/${id}`}
        className={project_container}
        ref={ref}
        style={{
          opacity: isDragging || isOver ? 0.3 : 1,
          cursor: isDragging ? "move" : "default",
        }}
      >
        <p className={card_title}>{name}</p>
        <p>Assigned ToDos: {todos.length}</p>
        <div className={user_icon}>{user_id}</div>
      </Link>

      {/* Delete and Edit buttons ------------ */}
      {!!id && (
        <span>
          <input
            type="button"
            value="Delete"
            className={`btn btn-warning btn-sm ${delete_btn}`}
            onClick={() => RemoveProject(id)}
          />

          <Link to={`/edit/${id}`}>
            <input
              type="button"
              value="Edit"
              className={`btn btn-warning btn-sm ${edit_btn}`}
            />
          </Link>
        </span>
      )}
    </div>
  );
}

const mapStateToProps = (state) => ({ projects: state.projects });
export default connect(mapStateToProps)(Project);

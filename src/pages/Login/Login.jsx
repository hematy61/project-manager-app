import React, { useState } from "react";
import { useHistory, useLocation, Link } from "react-router-dom";
import { fakeAuth } from "../../routes/PrivateRoute";
import { connect, useDispatch } from "react-redux";
import Envelope from "../../assets/icons/Envelope";
import {
  login_container,
  login_right,
  style_error,
  signup,
  is_valid,
  is_invalid,
} from "./Login.module.css";

function Login(props) {
  // hooks
  const dispatch = useDispatch();
  let history = useHistory();
  let location = useLocation();

  // states
  const [email, setEmail] = useState("");
  const [isEmailValid, setIsEmailValid] = useState();
  const [emailError, setEmailError] = useState("");
  const [error, setError] = useState("");
  const [userExist, setUserExist] = useState(false);
  const [pass, setPass] = useState("");

  // capturing the rerouted page location
  let { from } = location.state || { from: { pathname: "/" } };

  // -------------------------------------
  const handleSubmit = (e) => {
    e.preventDefault();

    const data = props.user;
    console.log("data", data);

    if (!data) {
      setUserExist(true);
      setError(
        "The account doesn't exist. Enter a different account or create one."
      );
      return;
    }

    if (data.email === email && data.pass === pass) {
      data.isLoggedIn = true;
      dispatch({ type: "save_user", payload: data });
      fakeAuth.authenticate(() => {
        history.replace(from);
      });
    }
  };

  // -------------------------------------
  const validateEmail = (e) => {
    e.persist();
    if (e.target.validity.valid) {
      setIsEmailValid(false);
      setEmailError("");
    } else {
      setUserExist(false);
      setIsEmailValid(true);
      setEmailError("Email is not Valid.");
    }
  };

  // -------------------------------------
  return (
    <div className={login_container}>
      <div className={login_right}>
        <h1>Log in</h1>

        <form onSubmit={handleSubmit} noValidate>
          <div className="form-group">
            {userExist && (
              <span className={style_error} aria-live="polite">
                {error}
              </span>
            )}

            <label htmlFor="emailAddress">Email address</label>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">
                  <Envelope />
                </span>
              </div>

              <input
                required
                type="email"
                className={`form-control ${
                  !isEmailValid
                    ? `is-valid ${is_valid}`
                    : `is-invalid ${is_invalid}`
                }`}
                id="emailAddress"
                aria-describedby="emailHelp"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={validateEmail}
                onFocus={() => setUserExist(false)}
              />
            </div>
          </div>

          {isEmailValid && (
            <span className={style_error} aria-live="polite">
              {emailError}
            </span>
          )}

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              required
              type="password"
              className="form-control"
              id="password"
              onChange={(e) => setPass(e.target.value)}
            />
          </div>

          <div className={signup}>
            <p>
              No account? no worries!
              <Link to="/signup"> create one</Link>
            </p>
          </div>

          <button type="submit" className="btn btn-primary">
            Log in
          </button>
        </form>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ user: state.user });
export default connect(mapStateToProps)(Login);

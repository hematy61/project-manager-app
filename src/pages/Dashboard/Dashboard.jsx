import React, { useCallback } from "react";
import { container, projects_container } from "./Dashboard.module.css";
import Project from "../../components/ProjectCard/ProjectCard";
import { Tools } from "../../components/Tools/Tools";
import { connect, useDispatch } from "react-redux";

function Dashboard(props) {
  // hooks
  const dispatch = useDispatch();

  const moveProject = useCallback(
    (dragIndex, hoverIndex) => {
      const dragProject = props.projects[dragIndex];

      const filteredProjects = props.projects.filter(
        (project) => project.id !== dragProject.id
      );

      dispatch({
        type: "move_project",
        payload: [
          ...filteredProjects.slice(0, hoverIndex),
          dragProject,
          ...filteredProjects.slice(hoverIndex),
        ],
      });
    },
    [props.projects, dispatch]
  );

  const onRemoveAll = () => {
    dispatch({ type: "remove_all" });
  };

  const handleSort = (e) => {
    const sortBy = e.target.innerText;
    let projects = props.projects.slice();

    if (sortBy === "Project Name(A-Z)") {
      projects.sort((a, b) => {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        } else if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    } else if (sortBy === "Project Name(Z-A)") {
      projects.sort((a, b) => {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA > nameB) {
          return -1;
        } else if (nameA < nameB) {
          return 1;
        }
        return 0;
      });
    }
    console.log(projects);

    dispatch({
      type: "sort_projects",
      payload: projects,
    });
  };

  return (
    <div className={container}>
      <h1>Project Manager</h1>

      <Tools onRemoveAll={onRemoveAll} handleSort={handleSort} />

      <div className={projects_container}>
        {props.projects.length === 0 ? (
          <Project project={[]} />
        ) : (
          props.projects.map((project, idx) => {
            return (
              <Project
                key={project.id}
                index={idx}
                project={project}
                moveProject={moveProject}
              />
            );
          })
        )}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ projects: state.projects });
export default connect(mapStateToProps)(Dashboard);

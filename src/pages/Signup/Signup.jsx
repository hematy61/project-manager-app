import React, { useState, useEffect } from "react";
import Envelope from "../../assets/icons/Envelope";
import Alert from "../../components/Alert/Alert.jsx";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  signup_container,
  signup_right,
  password_error,
} from "./Signup.module.css";

function Signup() {
  // hooks
  let dispatch = useDispatch();
  let history = useHistory();

  // states
  const [email, setEmail] = useState("");
  const [passOne, setPassOne] = useState("");
  const [passTwo, setPassTwo] = useState("");
  const [isPassMatch, setIsPassMatch] = useState(false);

  // initializing state and save it in Session Storage
  useEffect(() => {
    dispatch({ type: "INIT_STATE" });
  }, []);

  // --------------------------------------------
  const handlePassMatch = (e) => {
    if (passOne !== passTwo) {
      setIsPassMatch(true);
    }
  };

  // --------------------------------------------
  const resetError = () => {
    setIsPassMatch(false);
  };

  // --------------------------------------------
  const handleSubmit = (e) => {
    e.preventDefault();

    const user = { email, pass: passOne };
    dispatch({ type: "save_user", payload: user });

    history.push("/login");
  };

  // --------------------------------------------
  return (
    <div className={signup_container}>
      <div className={signup_right}>
        <h1>Sign up</h1>
        <p>Let's get you an account to start discovering Project Manager</p>

        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="emailAddress">Email address</label>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">
                  <Envelope />
                </span>
              </div>
              <input
                required
                type="email"
                className="form-control"
                id="emailAddress"
                aria-describedby="emailHelp"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <small id="emailHelp" className="form-text text-warning">
              We'll never share your email with anyone else.
            </small>
          </div>

          <div className="form-group">
            <label htmlFor="passwordOne">Password</label>
            <input
              required
              type="password"
              className="form-control"
              id="passwordOne"
              minLength="2"
              onChange={(e) => setPassOne(e.target.value)}
              onFocus={resetError}
              onBlur={handlePassMatch}
            />
          </div>

          <div className="form-group">
            <label htmlFor="passwordTwo">Retype Password</label>
            <input
              required
              type="password"
              className="form-control"
              id="passwordTwo"
              minLength="2"
              onChange={(e) => setPassTwo(e.target.value)}
              onBlur={handlePassMatch}
              onFocus={resetError}
            />
            <div className={password_error}>
              {isPassMatch && (
                <Alert
                  content="Password does not match."
                  color="alert-danger"
                />
              )}
            </div>
          </div>

          <button
            type="submit"
            className="btn btn-primary"
            disabled={isPassMatch || !email || !passOne || !passTwo}
          >
            Sign me up
          </button>
        </form>
      </div>
    </div>
  );
}

// const mapStateToProps = (state) => {
//   console.log(state);
//   return { user: state.user };
// };
// export default connect(mapStateToProps)(Signup);
export default Signup;
